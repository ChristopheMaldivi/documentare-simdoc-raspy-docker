FROM sdhibit/rpi-raspbian

MAINTAINER Christophe Maldivi "christophe.maldivi@orange.com"

ENV http_proxy http://proxy:8080
ENV https_proxy http://proxy:8080

RUN apt-get update && apt-get -y upgrade && apt-get install -y openjdk-8-jdk bzip2 libgvc6 libgts-0.7-5 && apt-get clean

# INSTALL GRAPHVIZ
RUN mkdir -p /opt/local
WORKDIR /opt/local
COPY graphviz-2.40.1-raspberry.tar.bz2 .
RUN tar xvjf graphviz*.tar.bz2 && rm graphviz*.tar.bz2 && mv opt/local/* . && rm -rf opt

# TEST GRAPHVIZ
RUN /opt/local/bin/dot -V
RUN /opt/local/bin/gvmap -V

# RUN AS USER
RUN useradd -ms /bin/bash user
USER user
WORKDIR /home/user

ENV LC_ALL C.UTF-8

# Install our application
COPY *.jar .

# Expose server port
EXPOSE 8080

ADD run.sh run.sh

# Start app
CMD ./run.sh
